export default {
  namespaced: true,
  state: {
    favorites: []
  },
  getters: {},
  actions: {
    addToFavorites({ commit }, movie) {
      commit('addToFavorites', movie);
    },
    removeFromFavorites({ commit }, movie) {
      commit('removeFromFavorites', movie);
    }
  },
  mutations: {
    addToFavorites(state, movie) {
      state.favorites.push(movie);
    },
    removeFromFavorites(state, movie) {
      const i = state.favorites.map(item => item.imdbID).indexOf(movie.imdbID);
      state.favorites.splice(i, 1);
    }
  }
};
