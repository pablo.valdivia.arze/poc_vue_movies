import Vue from 'vue';
import VueRouter from 'vue-router';
import MovieCardContainer from '@/components/containers/MovieCardContainer';
import SearchMovieContainer from '@/components/containers/SearchMovieContainer';
import FavoriteMoviesContainer from '@/components/containers/FavoriteMoviesContainer';

Vue.use(VueRouter);

export default new VueRouter({
  routes: [
    {
      path: '/',
      name: 'Home',
      redirect: 'search/indiana',
      component: SearchMovieContainer
    },
    {
      path: '/movie/:id',
      name: 'Movie',
      props: true,
      component: MovieCardContainer
    },
    {
      path: '/search/:name',
      name: 'SearchMovie',
      props: true,
      component: SearchMovieContainer
    },
    {
      path: '/favorites',
      name: 'FavoriteMovies',
      component: FavoriteMoviesContainer
    }
  ],
  mode: 'history'
});
