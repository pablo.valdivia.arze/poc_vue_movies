const baseUrl =
  'http://www.omdbapi.com/?apikey=67e6e6f5&type=movie&Content-Type=application/json';

export default {
  fetchMovieCollection(name, page) {
    return fetch(baseUrl + '&page=' + page + '&s=' + name).then(response =>
      response.json()
    );
  },

  fetchSingleMovie(id) {
    return fetch(baseUrl + '&i=' + id).then(response => response.json());
  }
};
